package com.webflux.service;

import com.webflux.dto.UserDto;
import com.webflux.dto.requestparams.UserRequestParamsDto;
import com.webflux.exception.DataNotFoundException;
import com.webflux.mongo.dao.UserDao;
import com.webflux.mongo.entity.User;
import com.webflux.mongo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDao userDao;

    // TODO get flux of filtered results
    public Flux<User> getUser(UserRequestParamsDto requestParamsDto) {
        return userDao.getUser(requestParamsDto);
    }

    public Mono<UserDto> createUser(UserDto userDto) {
        return userRepository.save(this.convertDtoToModel(userDto))
                        .map(savedUser -> {
                                    userDto.setId(savedUser.getId());
                                    return userDto;
                                }
                            );
    }

    public Mono<UserDto> updateUser(UserDto userDto) {
        Mono<UserDto> userMono = userRepository.findById(userDto.getId())
                .switchIfEmpty(Mono.error(new DataNotFoundException("User not found for #" + userDto.getId())))
                .map(user -> {
           userDto.setCreatedAt(user.getCreatedAt());
           userDto.setUpdatedAt(new Date()
           );

           return userDto;
        });

        userMono.subscribe(user -> userRepository.save(this.convertDtoToModel(user)));

        return userMono;
    }

    private User convertDtoToModel(UserDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setId(userDto.getId());
        user.setActive(userDto.getActive());
        user.setCreatedAt(userDto.getCreatedAt());
        user.setUpdatedAt(userDto.getUpdatedAt());

        return user;
    }

    private UserDto convertModelToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setEmail(user.getEmail());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setId(user.getId());
        userDto.setActive(user.getActive());
        userDto.setCreatedAt(user.getCreatedAt());
        userDto.setUpdatedAt(user.getUpdatedAt());

        return userDto;
    }
}
