package com.webflux.component;

import com.webflux.dto.error.ErrorDetailsDto;
import com.webflux.exception.DataValidationException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.SmartValidator;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class ObjectValidator {
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private SmartValidator validator;

    public <T> void validateObjectList(@NonNull List<T> referenceList, @NonNull String reference, @NonNull Class<?>... validationGroups) throws DataValidationException {
        // assert checks if indicator value list is not empty or not null
        Assert.isTrue(!referenceList.isEmpty(), "referenceList cannot be empty");
        Assert.isTrue(!reference.isEmpty(), "reference cannot be empty");

        String referenceObject = reference.substring(0, 1).toLowerCase() + reference.substring(1);

        List<ErrorDetailsDto> errorDetails = new ArrayList<>();
        for (Integer index = 0; index < referenceList.size(); index++) {
            // creating new binding result for each object
            BindingResult bindingResult = new BeanPropertyBindingResult(referenceList.get(index), String.format("%s[%s]", referenceObject, index));
            // spring smart validator binds the binding result with any errors
            validator.validate(referenceList.get(index), bindingResult, (Object[]) validationGroups);

            // if any errors from binding result
            if (bindingResult.hasErrors()) {
                // gets all the error messages
                errorDetails.add(this.getFieldErrors(referenceList.get(index), bindingResult));
            }
        }

        if (errorDetails.size() > 0) { // throws errors IFF errorDetails is not empty
            throw new DataValidationException(errorDetails);
        }
    }

    // get messages from the errors from binding result
    private ErrorDetailsDto getFieldErrors(Object object, BindingResult errors) {
        ErrorDetailsDto errorDetail = new ErrorDetailsDto();
        List<FieldError> fieldErrors = errors.getFieldErrors();
        List<String> messages = new ArrayList<>();
        for (FieldError fieldError : fieldErrors) {
            String fieldName = fieldError.getField().replaceAll("\\[(.*?)\\]", "");
            String message = fieldError.getDefaultMessage();
            try {
                if (message.contains("{0}")) {
                    message = MessageFormat.format(message, fieldError.getRejectedValue());
                }
                log.info("message: {}", (fieldError.getObjectName() + "." + fieldName + "." + fieldError.getCode()).replaceAll("\\[(.*?)\\]", ""));
                message = messageSource.getMessage(
                        (fieldError.getObjectName() + "." + fieldName + "." + fieldError.getCode()).replaceAll("\\[(.*?)\\]", ""),
                        fieldError.getRejectedValue() != null ? new String[]{fieldError.getRejectedValue().toString()} : null,
                        null
                );
            } catch (Exception e) {
                log.info("message not found for {}", fieldError.getObjectName() + "." + fieldName + "." + fieldError.getCode());
            }
            messages.add(message);
        }
        errorDetail.setError(String.join(", ", messages));
        errorDetail.setValue(object);
        return errorDetail;
    }
}
