package com.webflux.dto.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponseDto<T> {
    private String type;
    private List<T> errors;

    public ErrorResponseDto() {
    }

    public ErrorResponseDto(String type, List<T> errors) {
        this.type = type;
        this.errors = errors;
    }
}
