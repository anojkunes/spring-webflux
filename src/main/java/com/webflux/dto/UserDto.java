package com.webflux.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.webflux.mongo.entity.Base.UserBase;
import com.webflux.validation.group.CreateGroup;
import com.webflux.validation.group.UpdateGroup;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class UserDto extends UserBase {

    @Override
    @NotNull(groups = { UpdateGroup.class })
    public Long getId() { return super.getId(); }

    @Override
    @NotNull(groups = { CreateGroup.class, UpdateGroup.class })
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    @NotNull(groups = { CreateGroup.class, UpdateGroup.class })
    public String getLastName() {
        return super.getLastName();
    }

    @Override
    @NotNull(groups = { CreateGroup.class, UpdateGroup.class })
    @Email(groups = { CreateGroup.class, UpdateGroup.class })
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    @NotNull(groups = UpdateGroup.class)
    public Date getUpdatedAt() { return super.getUpdatedAt(); }

    @Override
    @NotNull(groups = { UpdateGroup.class })
    public Boolean getActive() { return super.getActive(); }

    @JsonIgnore
    public String getSequenceName() {
        return super.getSequenceName();
    }
}
