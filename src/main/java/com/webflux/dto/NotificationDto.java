package com.webflux.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.webflux.mongo.entity.Base.NotificationBase;

public class NotificationDto extends NotificationBase {
    @JsonIgnore
    public String getSequenceName() {
        return super.getSequenceName();
    }
}
