package com.webflux.dto.requestparams;

import lombok.Data;

@Data
public class UserRequestParamsDto {
    private String email;
    private String fullName;
    private Boolean active;
}
