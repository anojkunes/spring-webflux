package com.webflux._config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
public class CorsFilterConfig implements WebFluxConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("Access-Control-Allow-Headers", " Content-Type, Authorization")
                .allowedOrigins("*")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedMethods("OPTIONS", "GET", "PUT", "POST", "DELETE");
    }
}