package com.webflux.mongo.event;

import com.webflux.mongo.dao.SequenceGeneratorDao;
import com.webflux.mongo.entity.Base.AbstractUniqueIdentifierBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

@Component
public class MongoModelEventListener<T extends AbstractUniqueIdentifierBase> extends AbstractMongoEventListener<T> {
    @Autowired
    private SequenceGeneratorDao sequenceGeneratorDao;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<T> model) {
        boolean condition = model.getSource().getId() == null ||
                (model.getSource().getId() != null && model.getSource().getId() < 1L);
        if(condition) {
            model.getSource().setId(
                    sequenceGeneratorDao.generateSequence(model.getSource().getSequenceName())
            );
        }
    }
}
