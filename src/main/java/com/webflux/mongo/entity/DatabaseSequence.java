package com.webflux.mongo.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class DatabaseSequence {
    @Id
    private String id;
    private Long sequence;
}
