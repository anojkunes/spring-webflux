package com.webflux.mongo.entity;

import com.webflux.mongo.entity.Base.NotificationBase;
import lombok.Data;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Notification extends NotificationBase {
    @Override
    @Transient // Ignore property from being persisted
    public final String getSequenceName() {
        return "notifications_sequence";
    }
}
