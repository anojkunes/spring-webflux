package com.webflux.mongo.entity;

import com.webflux.mongo.entity.Base.UserBase;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User extends UserBase {
    @Override
    @Transient // Ignore property from being persisted
    public final String getSequenceName() {
        return "users_sequence";
    }
}