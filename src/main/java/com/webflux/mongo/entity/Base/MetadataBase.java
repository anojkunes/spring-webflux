package com.webflux.mongo.entity.Base;

import lombok.Data;

import java.util.Date;

@Data
public class MetadataBase extends AbstractUniqueIdentifierBase {
    protected Boolean active = Boolean.TRUE;
    protected Date createdAt = new Date();
    protected Date updatedAt = new Date();

    @Override
    public String getSequenceName() {
        throw new UnsupportedOperationException("sequence name is unsupported");
    }
}
