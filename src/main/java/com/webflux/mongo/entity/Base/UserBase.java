package com.webflux.mongo.entity.Base;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
public class UserBase extends MetadataBase {
    private String firstName;
    private String lastName;
    @Indexed(unique = true)
    private String email;
}
