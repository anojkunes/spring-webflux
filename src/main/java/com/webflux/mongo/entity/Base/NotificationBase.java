package com.webflux.mongo.entity.Base;

import lombok.Data;

@Data
public class NotificationBase extends MetadataBase {
    private Long userId;
    private String message;
}
