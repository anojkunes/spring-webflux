package com.webflux.mongo.entity.Base;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public abstract class AbstractUniqueIdentifierBase {
    @Id
    protected Long id;
    public abstract String getSequenceName();
}
