package com.webflux.mongo.dao;


import com.webflux.dto.requestparams.UserRequestParamsDto;
import com.webflux.mongo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public class UserDao {
    @Autowired
    public ReactiveMongoTemplate reactiveMongoTemplate;
    @Autowired
    public ReactiveMongoOperations reactiveMongoOperations;

    // todo add filter options
    public Flux<User> getUser(final UserRequestParamsDto userRequestParamsDto) {
        Query query = new Query();

        if (userRequestParamsDto.getEmail() != null) {
            query.addCriteria(Criteria.where("email"));
        }

        if (userRequestParamsDto.getActive() != null) {
            query.addCriteria(Criteria.where("active"));
        }

        if (userRequestParamsDto.getFullName() != null) {
            query.addCriteria(Criteria.where("fullName"));
        }

//        query.addCriteria(Criteria.where("email").is("anoj@transformingsystems.com"));
        return reactiveMongoTemplate.tail(query, User.class);
    }
}
