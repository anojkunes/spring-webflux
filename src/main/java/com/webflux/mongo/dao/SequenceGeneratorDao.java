package com.webflux.mongo.dao;

import com.webflux.mongo.entity.DatabaseSequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

// based on https://www.baeldung.com/spring-boot-mongodb-auto-generated-field
// Maybe create starter library for sequence generator
@Repository
public class SequenceGeneratorDao {
    @Autowired
    private MongoOperations mongoOperations;

    public Long generateSequence(String sequenceName) {
        DatabaseSequence databaseSequence = mongoOperations.findAndModify(
                Query.query(
                        Criteria.where("_id").is(sequenceName)
                ),
                new Update().inc("sequence", 1),
                FindAndModifyOptions.options().returnNew(true).upsert(true),
                DatabaseSequence.class
        );

        return databaseSequence != null ? databaseSequence.getSequence() : 1L;
    }
}
