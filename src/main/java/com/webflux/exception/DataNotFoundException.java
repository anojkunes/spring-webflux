package com.webflux.exception;

import lombok.Data;

@Data
public class DataNotFoundException extends RuntimeException {
    private String error;

    public DataNotFoundException(String error) {
        super(error);
        this.error = error;
    }
}
