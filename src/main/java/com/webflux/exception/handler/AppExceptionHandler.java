package com.webflux.exception.handler;

import com.webflux.component.DataBufferWriter;
import com.webflux.dto.error.ErrorDetailsDto;
import com.webflux.dto.error.ErrorResponseDto;
import com.webflux.exception.DataValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
@Order(-2)
public class AppExceptionHandler implements ErrorWebExceptionHandler {
    @Autowired
    private DataBufferWriter dataBufferWriter;

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        if(ex instanceof DataValidationException) {
            return this.handleException(exchange, (DataValidationException) ex);
        } else if (ex instanceof ServerWebInputException) {
            return this.handleException(exchange, (ServerWebInputException) ex);
        } else {
            return this.handleException(exchange, ex);
        }
    }

    private Mono<Void> handleException(ServerWebExchange exchange, ServerWebInputException ex) {
        List<ErrorDetailsDto> errorDetailsDto = Arrays.asList(
                new ErrorDetailsDto(null, ex.getMessage())
        );
        ErrorResponseDto errorResponseDto = new ErrorResponseDto();
        errorResponseDto.setErrors(errorDetailsDto);
        errorResponseDto.setType("JSON Parse Error");
        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);

        return dataBufferWriter.write(exchange.getResponse(), errorResponseDto);
    }

    private Mono<Void> handleException(ServerWebExchange exchange, DataValidationException ex) {
        List<ErrorDetailsDto> errorDetailsDto = ex.getErrors();
        ErrorResponseDto errorResponseDto = new ErrorResponseDto();
        errorResponseDto.setErrors(errorDetailsDto);
        errorResponseDto.setType("Validation Failed");
        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);

        return dataBufferWriter.write(exchange.getResponse(), errorResponseDto);
    }

    private Mono<Void> handleException(ServerWebExchange exchange, Throwable ex) {
        ErrorResponseDto errorResponseDto = new ErrorResponseDto(
                "Internal Server Error",
                Arrays.asList(new ErrorDetailsDto(null, ex.getMessage()))
        );

        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        exchange.getResponse().setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);

        return dataBufferWriter.write(exchange.getResponse(), errorResponseDto);
    }
}
