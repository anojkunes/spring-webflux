package com.webflux.exception;

import com.webflux.dto.error.ErrorDetailsDto;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import java.util.List;

public class DataValidationException extends RuntimeException {
    private List<ErrorDetailsDto> errors;

    public DataValidationException(List<ErrorDetailsDto> errors) {
        super("Validation Exception occurred");
        this.errors = errors;
    }

    public List<ErrorDetailsDto> getErrors() {
        return this.errors;
    }
}
