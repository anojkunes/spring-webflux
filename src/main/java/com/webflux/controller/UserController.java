package com.webflux.controller;

import com.webflux.component.ObjectValidator;
import com.webflux.dto.UserDto;
import com.webflux.dto.requestparams.UserRequestParamsDto;
import com.webflux.mongo.entity.User;
import com.webflux.service.UserService;
import com.webflux.validation.group.CreateGroup;
import com.webflux.validation.group.UpdateGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;

@RestController
@RequestMapping("/notification-service/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectValidator validator;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public ResponseEntity<Flux<User>> getUsers(UserRequestParamsDto requestParamsDto) {
        return new ResponseEntity(this.userService.getUser(requestParamsDto), HttpStatus.OK);
    }

    // based on https://inneka.com/programming/spring/spring-webflux-bean-validation-not-working/
    @PostMapping
    public ResponseEntity<Mono<UserDto>> createUser(@RequestBody UserDto userDto) {

        Mono<UserDto> userDtoMono = Mono.just(userDto)
                .doOnNext(user -> {
                    validator.validateObjectList(
                            Arrays.asList(user),
                            UserDto.class.getSimpleName(),
                            CreateGroup.class
                    );
                })
                .flatMap(user -> this.userService.createUser(user));

        return new ResponseEntity(userDtoMono, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Mono<UserDto>> updateUser(@RequestBody UserDto userDto) {

        Mono<UserDto> userDtoMono = Mono.just(userDto)
                .doOnNext(user -> {
                    validator.validateObjectList(
                            Arrays.asList(user),
                            UserDto.class.getSimpleName(),
                            UpdateGroup.class
                    );
                })
                .flatMap(user -> this.userService.updateUser(user));

        return new ResponseEntity(userDtoMono, HttpStatus.OK);
    }
}
